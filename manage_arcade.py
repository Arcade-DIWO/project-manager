from flask_script import Manager, Server
from server import app

manager = Manager(app)

# Turn on debugger by default and reloader
manager.add_command("run", Server(
    use_debugger = True,
    use_reloader = True,
    threaded = True,
    host = 'localhost',
    port = '8000')
)


if __name__ == "__main__":
    manager.run()
