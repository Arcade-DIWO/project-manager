# -*- coding: utf-8 -*-
from server import db
import time

# http://flask-sqlalchemy.pocoo.org/2.3/quickstart/

class PlayHistory(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    game_name = db.Column(db.String(254))
    start_time = db.Column(db.Integer)
    duration = db.Column(db.Integer)

    def __init__(self, game_name):
        self.game_name = game_name
        self.start_time=None
        self.duration=None

    def start(self):
        if not self.start_time:
            self.start_time=int(time.time())
            db.session.add(self)
            db.session.commit()
        
    def stop(self):
        if not self.duration:
            self.duration=int(time.time())-self.start_time
            db.session.commit()
