# -*- coding: utf-8 -*-
from flask import request, Response
from threading import Thread
from server import app
from .models import PlayHistory
import os, json
import subprocess
from lxml import etree
import cgi
import html

os.environ['DISPLAY'] = ':0'


@app.route('/', methods=['GET'])
def index():
    return app.send_static_file('index.html')


@app.route('/listProjects', methods=['GET'])
def list_project():
    projects = []
    for projectDir in ['local', 'common']:
        (path, dirs, files) = next(os.walk(os.path.join(app.config['PROJECT_PATH'], projectDir), topdown=True))
        for project in files:
            try: # we may have a corrupted project file
                tree = etree.parse(open(os.path.join(path, project)))			
                thumbnail = tree.findtext(".//thumbnail")
                notes = tree.findtext(".//notes")

                #escape strings for display in HTML
                notes = html.escape(notes, quote=True).\
                        replace(u'\r', u'<br />').\
                        replace(u'  ', u' &nbsp;')
                
                projects.append({   'name':project[:-4],
                                    'path':projectDir,
                                    'thumbnail':thumbnail,
                                    'notes':notes})
            except:
                pass

    projects.sort(key=lambda d: (d['name']))
    projects.sort(key=lambda d: (d['path']), reverse=True)
    data = {'title': app.config['TITLE'], 'projects': projects}
    return JsonResponse(json.dumps(data))    

@app.route('/start/<string:path>/<string:projectName>', methods=['GET'])
def start_project(path, projectName):
    
    projectURL = "http://localhost:8000/static/projects/%s/%s.xml" % (path, projectName)
    url = "http://localhost:8000/static/snap/snap.html#run:%s&hideControls&noExitWarning" % projectURL
    
    if app.config['BROWSER_BIN'] == "/usr/bin/chromium":
        executable = [app.config['BROWSER_BIN'], '--kiosk', url]
        #process = subprocess.call(executable, stdout=subprocess.PIPE, shell=False)
        subprocess.Popen(executable)
    
    if app.config['BROWSER_BIN'] == "/usr/bin/firefox":
        executable = [app.config['BROWSER_BIN'], '--kiosk', url]
        #executable = [app.config['BROWSER_BIN'], '--kiosk', '--private-window', '--safe-mode', url]
        #executable = [app.config['BROWSER_BIN'], '--kiosk', url]
        #executable = [app.config['BROWSER_BIN'], url]
        process = subprocess.call(executable, stdout=subprocess.PIPE, shell=False)
        
    
    """
    def run_project():
        DEV_NULL = open(os.devnull, 'wb', 0)
        
        gamePlayLog=PlayHistory(projectName)
        gamePlayLog.start()
        executable = ['/usr/bin/chromium', url, '-kiosk', '--incognito']
        try:
            subprocess.check_call(executable, stdout=DEV_NULL, stderr=DEV_NULL)
        except:
            # 'pkill chromium' raises an expection
            pass
        gamePlayLog.stop()
        print("{} played for {} seconds".format(gamePlayLog.game_name, gamePlayLog.duration))
        
    thread = Thread(target=run_project)
    thread.start()
    """
    return JsonResponse()    

@app.route('/volume-up', methods=['GET'])
def volume_up():
    pass

@app.route('/volume-down', methods=['GET'])
def volume_down():
    pass

def JsonResponse(json_response="1", status_code=200):
    response = Response(json_response, 'application/json; charset=utf-8')
    response.headers.add('content-length', len(json_response))
    response.status_code=status_code
    return response
