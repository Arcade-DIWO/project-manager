#!/bin/bash

sleep 30

#update software
echo -e "GET http://google.com HTTP/1.0\n\n" | nc google.com 80 > /dev/null 2>&1
if [ $? -eq 0 ]; then
    # we have network
	cd /home/arcade/arcade
	git pull origin master
	cd /home/arcade/arcade/projects/common
	git pull origin master
	cd /home/arcade/arcade/projects/local
	git pull origin master
    
    chown -R arcade /home/arcade/arcade
    
    touch /tmp/git-repos.updated
else
    echo $? > /tmp/not-updated.no-network
fi
